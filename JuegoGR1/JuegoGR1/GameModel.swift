//
//  GameModel.swift
//  JuegoGR1
//
//  Created by Juan Pa on 14/11/17.
//  Copyright © 2017 Juan Pa. All rights reserved.
//

import Foundation

class GameModel{
    
    var puntaje:Int = 0
    var ronda:Int = 1
    var objetivo:Int?
    
    init(){
    
    setObjetivo()
    
    }
    
    func setObjetivo(){
        
        objetivo = Int(arc4random_uniform(99) + 1)
        
    }
    
    func jugar(valorIntento: Int){
        
        
        puntaje += calcularPuntaje(valorIntento: valorIntento)
        ronda += 1
        setObjetivo()
        
    }
    
    func reiniciar(){
        
        puntaje = 0
        ronda = 1
        setObjetivo()
        
    }
    
    func calcularPuntaje(valorIntento:Int) -> Int{
        
        let diferencia = abs(valorIntento - objetivo!)
        
        switch diferencia {
        case 1...3:
            return 75
        
        case 4...10:
            return 50
        
        case 0:
            return 100
            
        default:
            return 0
        }
        
    }
    
}
