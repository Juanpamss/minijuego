//
//  ViewController.swift
//  JuegoGR1
//
//  Created by Juan Pa on 14/11/17.
//  Copyright © 2017 Juan Pa. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    //MARK:- Attributes
    
    let gameModel = GameModel()
    
    //MARK:- Outlets
    
    @IBOutlet weak var labelOrden: UILabel!
    
    @IBOutlet weak var labelObjetivo: UILabel!
    
    @IBOutlet weak var labelPuntaje: UILabel!
    
    @IBOutlet weak var labelRonda: UILabel!
    
    @IBOutlet weak var slider: UISlider!
    
    //MARK:- ViewController LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setValores()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:- Actions
    
    @IBAction func btnJugar(_ sender: Any) {
        
        gameModel.jugar(valorIntento: Int(round(slider.value)))
        setValores()
        
    }
    
    @IBAction func btnReiniciar(_ sender: Any) {
        
        gameModel.reiniciar()
        slider.value = 50
        setValores()
        
    }
    
    @IBAction func btnInfo(_ sender: Any) {
    }
    
    func setValores(){
        
        labelObjetivo.text = "\(gameModel.objetivo ?? 0)"
        labelPuntaje.text = "Puntaje: \(gameModel.puntaje)"
        labelRonda.text = "Ronda: \(gameModel.ronda)"
        slider.value = 50
        
    }
    
    
    
}

